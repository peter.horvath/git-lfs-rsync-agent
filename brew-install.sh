#!/opt/homebrew/bin/zsh -x
[ -d dist ] || mkdir -pv dist
git archive --format tar.gz HEAD >dist/git-lfs-rsync-agent.tar.gz
sed 's/%PWD%/'"$(echo "$PWD"|sed 's/\//\\\//g')"'/g' <git-lfs-rsync-agent.rb >dist/git-lfs-rsync-agent.rb
rm -vf /opt/homebrew/Library/Taps/homebrew/homebrew-core/Formula/git-lfs-rsync-agent.rb
brew developer on
brew install --build-bottle --formula ./dist/git-lfs-rsync-agent.rb
#brew create --go --set-name git-lfs-rsync-agent --set-version 1.0 file://$PWD/dist/git-lfs-rsync-agent.tar.gz
#brew reinstall --build-from-source --formula ./dist/git-lfs-rsync-agent.rb
