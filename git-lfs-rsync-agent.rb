class GitLfsRsyncAgent < Formula
  desc "Git Lfs over Ssh, using Rsync"
  homepage "https://gitlab.com/peter.horvath/git-lfs-rsync-agent"
  url "file:///%PWD%/dist/git-lfs-rsync-agent.tar.gz"
  version "1.0"
  sha256 "ec17e9f3af59668e66d6d9af2c800b9a16784a0881cc501459a0dd6f5cd29f92"
  license "GPL-3.0-or-later"

  depends_on "go" => :build

  def install
    system "go", "build", "-o", "git-lfs-rsync-agent", *std_go_args(ldflags: "-s -w")
  end


  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test git-lfs-rsync-agent`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
